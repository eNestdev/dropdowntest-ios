//
//  ViewController.swift
//  GenericDropDown
//
//  Created by PC-50 on 09/10/20.
//  Copyright © 2020 PC-50. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var selectedData: UILabel!
    
    // MARK: - Variable Declerations
    var country: DropDownClassViewController<Country>?
    var selectedValue = Country(id: 1, name: "Canada")
    let countries =  [Country(id: 0, name: "USA"),
                      Country(id: 1, name: "Canada"),
                      Country(id: 2, name: "United Kingdom")]
    let placholder = "Select Country"
    override func viewDidLoad() {
        super.viewDidLoad()
        handelCountryData()
    }
    
    func handelCountryData(){
        countryTextField.clearButtonMode = .whileEditing
        countryTextField.text = selectedValue.name
        self.selectedData.text = "Your Selected Country: \(selectedValue.name)"
        countryTextField.placeholder = placholder
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.country = DropDownClassViewController<Country>(
            withItems: countries,
            withRowTitle: { (country) -> String in
            return country.name
        },
            didSelect: { (country) in
                self.selectedValue = country
                self.selectedData.text = "Your Selected Country: \(self.selectedValue.name)"
                self.countryTextField.text = self.selectedValue.name
        }
        )
        countryTextField.setupPickerField(withDataSource: country!,selectedIndex: self.selectedValue.id)
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if textField == countryTextField {
            selectedData.text = ""
        }
        return true
    }
}
