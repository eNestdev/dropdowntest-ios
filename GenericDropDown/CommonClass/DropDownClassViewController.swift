//
//  DropDownClassViewController.swift
//  GenericDropDown
//
//  Created by PC-50 on 09/10/20.
//  Copyright © 2020 PC-50. All rights reserved.
//

import UIKit

class DropDownClassViewController<T>: NSObject,UIPickerViewDelegate,UIPickerViewDataSource {
    public var listItems: [T]
    public var items: [GenericRow<T>]
    public var selected: (T) -> Void
   

    public init(withItems listItems: [T], withRowTitle generateRowTitle: (T) -> String,  didSelect selected: @escaping (T) -> Void) {
        self.listItems = listItems
        self.selected = selected
        self.items = listItems.map {
            GenericRow<T>(type: $0, title: generateRowTitle($0))
        }
    }
    // MARK: - PickerView Functions
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return items.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       self.selected(items[row].type)
        return items[row].title
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selected(items[row].type)
    }

}
