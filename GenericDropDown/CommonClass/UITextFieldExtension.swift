//
//  File.swift
//  GenericDropDown
//
//  Created by PC-50 on 09/10/20.
//  Copyright © 2020 PC-50. All rights reserved.
//
import Foundation
import UIKit

extension UITextField {

    func setupPickerField<T>(withDataSource dataSource: DropDownClassViewController<T>,selectedIndex:Int) {
        let pickerView = UIPickerView()
        self.inputView = pickerView
        self.addDoneToolbar()
        pickerView.delegate = dataSource
        pickerView.dataSource = dataSource
        pickerView.reloadAllComponents()
        pickerView.selectRow(selectedIndex, inComponent: 0, animated: true)
    }

    func addDoneToolbar(onDone: (target: Any, action: Selector)? = nil) {
        let onDone = onDone ?? (target: self, action: #selector(doneButtonTapped))

        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.tintColor = UIColor.blue

        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: .done, target: onDone.target, action: onDone.action)
        ]
        toolbar.sizeToFit()
        self.inputAccessoryView = toolbar
    }

    // MARK: - Default actions:
    @objc private func doneButtonTapped() { self.resignFirstResponder() }
}
