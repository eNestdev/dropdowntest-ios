
//
//  File.swift
//  GenericDropDown
//
//  Created by PC-50 on 09/10/20.
//  Copyright © 2020 PC-50. All rights reserved.
//

struct Country {
    var id: Int,
    name: String
}

struct GenericRow<T> {
    let type: T
    let title: String
    public init(type: T, title: String) {
        self.type = type
        self.title = title
    }
}
